### Dots

### List of applications

# Before-anything-other manual install packages

 * pacaur

# [Core] packages

 * alacritty
 * alacritty-terminfo
 * alsa-firmware
 * alsa-plugins
 * alsa-tools
 * alsa-utils
 * alsaequal
 * bc
 * bluez
 * bluez-hid2hci
 * bluez-libs
 * bluez-plugins
 * bluez-tools
 * bluez-utils
 * chromium
 * cli-visualizer-git
 * ctpl
 * docker
 * dunst
 * exa
 * exiv2
 * feh
 * ffmpeg
 * ffmpegthumbnailer

# Next is vifm fuse support
 * fuse
 * fuseiso
 * fuse-zip
 * fuse-7z
 * archivemount
 * curl2ftpfs
 * rar2fs

 * fzf
 * gimp
 * gimp-nufraw
 * git
 * gradle
 * gst-plugins-bad
 * gst-plugins-base
 * gst-plugins-base-libs
 * gst-plugins-good
 * gst-plugins-ugly
 * gstreamer
 * imagemagick
 * inotify-tools
 * jdk8-openjdk
 * maim
 * maven
 * mpd
 * mpv
 * mupdf
 * neofetch
 * neomutt
 * neovim
 * nerd-fonts-complete
 * newsboat
 * ncmpcpp-vim-git
 * openssh
 * openssl
 * polybar
 * pulseaudio
 * pulseaudio-alsa
 * pulseaudio-bluetooth
 * python-ueberzug
 * rofi
 * rofi-power-menu
 * sshfs
 * skypeforlinux-stable-bin
 * slop
 * sudo
 * sxiv
 * sxiv-rifle
 * syntax-highlighting
 * tig
 * unclutter-xfixes-git
 * v4l-utils
 * vifm
 * w3m
 * wpa_supplicant
 * xclip
 * xdg-user-dirs
 * xdg-utils
 * xf86-video-amdgpu
 * xgetres
 * xkb-switch
 * xorg-server
 * xorg-xdpyinfo
 * xorg-xev
 * xorg-xinit
 * xsel
 * xsecurelock
 * xss-lock
 * youtube-dl
 * zathura
 * zathura-pdf-mupdf
 * zsh

# [wm] packages

 * awesome
 * i3

# [ide] packages

 * clion
 * datagrip
 * goland
 * intellij-idea-ultimate-edition
 * pycharm-professional
