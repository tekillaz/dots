#!/bin/bash

#
CDIR="$(dirname $(realpath $0))"
source "$CDIR/config"

#
COLORS="${1:-$ENVS/colors.env}"
INPUT="${2:-$TEMPLATES}"
OUTPUT="${3:-$VIEWS}"

#
[ ! -f "$COLORS" ] && echo "No colors file exists" && exit 1
[ ! -d "$INPUT"  ] && echo "No templates folder exists" && exit 1
[ ! -d "$OUTPUT" ] && mkdir -p "$OUTPUT"

# generate views
for f in $(find "$INPUT" -type f -print); do
    fn=$(realpath --relative-to=$INPUT $f)
    fn_in="$INPUT/$fn"
    fn_out="$OUTPUT/$fn"

    #
    mkdir -p "$(dirname $fn_out)"
    echo "[generate] $fn_in -> $fn_out"
    ctpl --env-file="$COLORS" "$fn_in" > "$fn_out"
done

# remove deleted views
# TODO:;

echo
