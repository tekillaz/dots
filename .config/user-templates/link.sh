#!/bin/bash

#
CDIR="$(dirname $(realpath $0))"
source "$CDIR/config"

#
VIEWS="${1:-$VIEWS}"
BASE="${2:-$BASE_DIRECTORY}"

#
[ ! -d "$VIEWS" ] && echo "No views folder exists" && exit 1
[ ! -d "$BASE"  ] && mkdir -p "$BASE" && echo "No base folder exists" && exit 1

#
for f in $(find "$VIEWS" -type f -print); do
    fn="$(realpath --relative-to=$VIEWS $f)"
    fn_in="$VIEWS/$fn"
    fn_out="$BASE/$fn"

    #
    if [[ ! -f "$fn_out" ]] || [[ ! "$fn_in" -ef "$fn_out" ]]; then
        mkdir -p "$(dirname $fn_out)"
        if [[ -h "$fn_out" ]] || [[ -e "$fn_out" ]]; then
            # already exists
            # need to backup file
            echo "[backup] $fn_out - exists, need to backup"

            #
            it=1
            fn_out_bak="$fn_out"
            while [[ -h "$fn_out_bak" ]] || [[ -e "$fn_out_bak" ]]; do
                fn_out_bak="$fn_out_bak.bak.$it"
                it=$(($it+1))
            done
            echo "[backup] $fn_out -> $fn_out_bak"
            mv "$fn_out" "$fn_out_bak"
        fi
        echo "[link] $fn_in -> $fn_out"
        ln -s "$fn_in" "$fn_out"
    fi
done
echo
