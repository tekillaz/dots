#!/bin/bash

#
CDIR="$(dirname $(realpath $0))"
source "$CDIR/config"

inotifywait -m -e close_write,create,delete --format '%e %w%f' -r "$VIEWS" |
    while read EVENT
    do
        echo ">> $EVENT"
        $CDIR/link.sh
    done
