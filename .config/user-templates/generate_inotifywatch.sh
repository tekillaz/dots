#!/bin/bash

#
CDIR="$(dirname $(realpath $0))"
source "$CDIR/config"

inotifywait -m -e close_write,create,delete --format '%e %w%f' -r "$ENVS" "$TEMPLATES" |
    while read EVENT
    do
        echo ">> $EVENT"
        $CDIR/generate.sh
    done
