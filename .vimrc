
"
"
"

set nocompatible    " use vim defaults instead of vi
set encoding=utf-8  " always encode in utf


" ------------- "
" -- Plugins -- "
" ------------- "
filetype off                          " required by vundle
set rtp+=~/.vim/bundle/Vundle.vim     " set the runtime path to include Vundle
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

" keyboard layout switcher
" https://github.com/lyokha/vim-xkbswitch
Plugin 'lyokha/vim-xkbswitch'

" base16 theme.
" https://github.com/chriskempson/base16#template-repositories
Plugin 'chriskempson/base16-vim'

" Aitline plugin
" https://github.com/vim-airline/vim-airline
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

" Fugitive plugin
" Used by airlines to show git branch name
" https://github.com/tpope/vim-fugitive
Plugin 'tpope/vim-fugitive'

" Fzf plugin
" https://github.com/junegunn/fzf.vim
Plugin 'junegunn/fzf.vim'

" Surround plugin
" https://github.com/tpope/vim-surround
Plugin 'tpope/vim-surround'

" i3 syntax
" https://github.com/mboughaba/i3config.vim
Plugin 'mboughaba/i3config.vim'

call vundle#end()



" -------------- "
" -- Settings -- "
" -------------- "

" Keyboard switcher
    let g:XkbSwitchEnabled = 1          " enable vim-xkbswitcher
    let g:XkbSwitchNLayout = 'us'       " default layout for insert mode
    let g:XkbSwitchIMappings = ['ru']   " enable insert mode mapping for layout different from eng.

" File detection
    syntax on
    filetype indent plugin on

" General
    set autoread                   " auto refresh any file not being edited by Vim
    set background=dark            " dark background
    set directory^=$HOME/backups/  " put all swap files together in one place
    set backspace=2                " enable <BS> for everything
    set completeopt-=preview       " dont show preview window
    set fillchars+=vert:\          " empty space instead of broken line for vsplits
    set hidden                     " hide when switching buffers, don't unload
    set laststatus=2               " always show status line
    set mouse=a                    " enable mouse in all modes
    set nowrap                     " word wrap
    set linebreak                  " attempt to wrap lines cleanly
    set number relativenumber      " show hybrid line numbers: relative + absolute
    set cursorline                 " highlight cursor line
    set title                      " use filename in window title
    set ttyfast                    " you've got a fast terminal

" Folding
    set foldignore=                " don't ignore anything when folding
    set foldlevelstart=99          " no folds closed on open
    set foldmethod=indent          " collapse code using indentations

" Tabs
    set autoindent                 " copy indent from previous line
    set expandtab                  " replace tabs with spaces
    set shiftwidth=4               " spaces for autoindenting
    set smarttab                   " <BS> removes shiftwidth worth of spaces
    set softtabstop=2              " spaces for editing, e.g. <Tab> or <BS>
    set tabstop=2                  " spaces for <Tab>
    set list                       " show tabs&spaces
    set lcs+=space:·               " mark sapces with this character

" Panes
    set splitbelow                 " Always open new vertical split below current
    set splitright                 " Always open new horizontal split righter current
    autocmd VimResized * wincmd =  " Automaticaly resize panels to equals size

" Searches
    set hlsearch                   " highlight search results
    set incsearch                  " search whilst typing
    set ignorecase                 " case insensitive searching
    set smartcase                  " override ignorecase if upper case typed
    set showcmd                    " show command on last line of screen
    set showmatch                  " show bracket matches
    set textwidth=0                " don't break lines after some maximum width
    set wildmenu                   " enhanced cmd line completion
    set ruler                      " shows ruler
    set clipboard=unnamedplus      " use the clipboards of vim and win
    set go+=a                      " Visual selection automatically copied to the clipboard

" Colours
    set termguicolors
    if filereadable(expand("~/.vimrc_background"))
        let base16colorspace=256
        source $HOME/.vimrc_background  " base16 colorscheme
    endif
    let g:airline_powerline_fonts = 1   " vim-airlines turn on powerline font glyph



" -------------- "
" -- Mappings -- "
" -------------- "

" Map leader
let mapleader = ','

" Exit insert mode
inoremap jj <esc>

" Toggle fold
"nnoremap <space> za

" Toggle spellcheck
"nnoremap <leader>s :set spell!<CR>

" Toggle hlsearch for current results
nnoremap <leader><leader> :nohlsearch<CR>

" Insert newline in normal mode
"nnoremap <S-o> O<Esc>

" Search for trailing whitespaces
"nnoremap <leader>w /\s\+$<CR>

" Remove trailing whitespaces
nnoremap <silent> <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR>

" Toggle method used for folding
nnoremap mm :call ToggleFoldMethod()<CR>

" Paste mode for terminals
"nnoremap <F2> :set invpaste paste?<CR>
"set pastetoggle=<F2>

" Yank (copy) selection to clipboard
nnoremap <C-c> "+y

" Put (paste) from clipboard
nnoremap <C-P> "*p

" Explore
nnoremap <leader>n :Sexplore!<CR>
nnoremap <leader>m :Hexplore<CR>
nnoremap <leader>l :Lexplore<CR>

" Shortcut for Tabularize
"nnoremap <leader>t :Tabularize /

" Panes
nnoremap <A-j> <C-W><C-J>
nnoremap <A-k> <C-W><C-K>
nnoremap <A-l> <C-W><C-L>
nnoremap <A-h> <C-W><C-H>
nnoremap <silent> <F6> <C-w><
nnoremap <silent> <F7> <C-w>>
nnoremap <silent> <F8> <C-w>-
nnoremap <silent> <F9> <C-w>+

" Swap top/bottom or left/right split                           : Ctrl+W R
" Break out current window into a new tabview                   : Ctrl+W T
" Close every window in the current tabview but the current one : Ctrl+W o

" Max out the height of the current split                       : Ctrl+W _
" Max out the width of the current split                        : Ctrl+W |
" Normalize all split sizes                                     : Ctrl+W =

" Page tabs
"nnoremap <C-n> :tabe<CR>:Explore!<CR>
"nnoremap <C-k> gt
"nnoremap <C-j> gT
"nnoremap <C-1> 1gt
"nnoremap <C-2> 2gt
"nnoremap <C-3> 3gt
"nnoremap <C-4> 4gt
"nnoremap <C-5> 5gt
"nnoremap <C-6> 6gt
"nnoremap <C-7> 7gt
"nnoremap <C-8> 8gt
"nnoremap <C-9> 9gt
"nnoremap <C-0> :tablast<CR>

" -- Other Settings -- "

let g:netrw_liststyle=3

" Disable arrow navigation
noremap  <Up>    ""
noremap! <Up>    <NOP>
noremap  <Down>  ""
noremap! <Down>  <NOP>
noremap  <Left>  ""
noremap! <Left>  <NOP>
noremap  <Right> ""
noremap! <Right> <NOP>

" Disbale page up/down, home&end navigation
noremap  <PageUp>   ""
noremap! <PageUp>   <NOP>
noremap  <PageDown> ""
noremap! <PageDown> <NOP>
noremap  <Home>     ""
noremap! <Home>     <NOP>
noremap  <End>      ""
noremap! <End>      <NOP>


" -- Functions -- "

function! ToggleFoldMethod()
if &foldmethod == 'indent'
  set foldmethod=marker
  echo "foldmethod=marker"
else
  set foldmethod=indent
  echo "foldmethod=indent"
endif
endfunction
