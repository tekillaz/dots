#!/bin/sh

# Profile file. Runs on login.
# Environmental variables are set here.

# Default WM
export WM="i3-gaps"

# golang path
export GOPATH="$HOME/Workspace/go"

# Add './local/bin' to $PATH
export PATH="$HOME/.local/bin:$PATH"
export PATH="$GOPATH/bin:$PATH"

# Default programs
export EDITOR="nvim"
export TERMINAL="alacritty"
export BROWSER="chromium"
export READER="zathura"
export FILEMANAGER="vifm"

